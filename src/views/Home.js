import React from 'react'
import Delay from '../components/Delay'
import Mountains from '../components/drawings/Mountains'
import Sun from '../components/drawings/Sun'
import simon from '../assets/simonx10.gif'

export default () =>
<div
  className='home container'
>
  <header className="app-header home">
    <Delay
      delay={5}
      duration={4}
      once={false}
      key='home'
      style={{
        height: '100%'
      }}
    >
      <div style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        height: '100%'
      }}>
        <h2>Hey there,</h2>
        <h1>my name's Sam</h1>
        <h2>I'm a Software Engineer in Portland, OR</h2>
      </div>
    </Delay>
    <div style={{
      position: "absolute",
      top: "calc(100% - 400px)"
    }}>
    <Sun
      className="drawing sun"
      stroke="orange"
      style={{
        position: "fixed",
        width: "300px",
        left: "calc(50% - 100px)",
        top: "calc(100% - 310px)",
      }}
      l={0} t={0} w={400} h={600}
    />
    <Mountains
      className="drawing"
      stroke="lightblue"
      style={{
        position: "fixed",
        width: "300px",
        left: "calc(50% - 180px)",
        top: "calc(100% - 370px)",
      }}
      l={0} t={0} w={400} h={600}
    />
    </div>
  </header>
</div>

const SimonPanel = () =>
  <Delay
    delay={3}
    duration={3}
    once={false}
    key='home-simon'
  >
    <div style={{display: 'flex'}}>
      <img
        className='home simon'
        src={simon}
        alt='this is a cat'
      />
    </div>
  </Delay>
