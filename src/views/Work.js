import React from 'react'
import Delay from '../components/Delay'

const linkedInUrl = "https://www.linkedin.com/in/sam-weller-354469161/"

export default () =>
  <header className="app-header work container">
    <Delay
      delay={2}
      duration={2}
      once={false}
      key='work'
    >
      <h2>What I do for work</h2>
      <Description/>
      <LinkedInSection/>
      <LetsConnect/>
      <SkillsSummary/>
    </Delay>
  </header>

const Description = () =>
<>
  <p>
    I'm a Full Stack Software Engineer. I've worked in Portland for about 5 years.
  </p>
  <p>
    I &hearts; the JS community & ecosystem, and I'm a big fan of the world of serverless.
  </p>
</>

const LetsConnect = () =>
<>
  <p>
    If you need someone to build your new app or service, let's connect!
  </p>
  <p>
    If you need someone to migrate your app to scalable architecture, I can help with that.
  </p>
</>

const LinkedInSection = () =>
<>
  <h2>
    <a href={linkedInUrl} target="_blank">
    Find me on LinkedIn
    </a>
  </h2>
</>

const SkillsSummary = () =>
<>
  <h2>
    What I can contribute
  </h2>
  <ul className="work-list">
    <li>
    Experience building highly available Single Page Applications supported by serverless infrastructure.
    </li>
    <li>
    Expertise in modern Javascript - ReactJS, NodeJS, ES6 - and Infrastructure as Code with Serverless Framework and AWS
    </li>
    <li>
    5 years of Full Stack experience across monolithic and micro service apps.
    </li>
  </ul>
</>
