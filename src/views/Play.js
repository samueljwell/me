import React from 'react'
import Delay from '../components/Delay'
import Me from '../assets/square-me-2.jpg'
import hobbyList from '../constants/hobbies'

export default () =>
<header className="app-header">
  <Delay
    delay={2}
    duration={2}
    once={false}
    key='play'
  >
    <h2>More about me</h2>
    <img 
      src={Me} 
      className="me-img"
    />
    <Bio/>
    <Hobbies/>
    <Song/>
  </Delay>
</header>

const Bio = () =>
<p>
  I'm a PNW native who enjoys messing with tech and spending time on my hobbies. Stuff like...
</p>

const Hobbies = () =>
  <div style={{textAlign: 'left'}}>
    {renderSection(hobbyList)}
  </div>

const renderSection = list =>
  <ul className="more-list">
    {list && list.map((item,i) => (
      <li
        key={i}>
        {item}
      </li>
    ))}
  </ul>

const Song = () =>
<>
<p style={{textAlign: "left"}}>
  Here's a recent recording.
  <br/>
  Hoping to get some more up soon.
</p>
<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/783215455&color=%23fd9ecf&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
</>
