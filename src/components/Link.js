import React from 'react' 
import { Link as RouterLink } from 'react-router-dom' 
 
const Link = (props) => ( 
  <RouterLink onClick={()=>window.scrollTo(0,0)} {...props} />
) 
 
export default Link 

