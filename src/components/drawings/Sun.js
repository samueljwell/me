import React from 'react'

const Sun = props =>
<svg viewBox={`${props.l} ${props.t} ${props.w} ${props.h}`} style={props.style} className="drawing-border">
<path className={props.className} stroke={props.stroke} fill="transparent" d="M 257 149 L 257 149 L 250 130 L 242 120 L 232 109 L 213 104 L 198 104 L 173 107 L 157 115 L 146 130 L 141 137 L 139 152 L 139 159 L 139 172 L 146 186 L 161 196 L 169 200 L 177 200 L 189 200 L 204 200 L 215 200 L 226 200 L 234 194 L 245 186 L 250 182 L 256 175 L 256 170 L 256 166">
</path>
<path className={props.className} stroke={props.stroke} fill="transparent" d="M 187 233 L 187 233 L 189 239 L 192 251 L 192 259 L 193 266 L 195 270">
</path>
<path className={props.className} stroke={props.stroke} fill="transparent" d="M 265 222 L 265 222 L 271 226 L 287 232 L 295 240 L 304 246">
</path>
<path className={props.className} stroke={props.stroke} fill="transparent" d="M 287 127 L 287 127 L 289 126 L 296 118 L 302 114 L 310 108 L 317 103 L 325 95">
</path>
<path className={props.className} stroke={props.stroke} fill="transparent" d="M 224 66 L 224 66 L 225 64 L 231 49 L 231 34 L 231 27">
</path>
<path className={props.className} stroke={props.stroke} fill="transparent" d="M 149 68 L 149 68 L 149 66 L 146 60 L 143 54 L 139 45">
</path>
<path className={props.className} stroke={props.stroke} fill="transparent"  d="M 123 125 L 123 125  L 97 112 L 89 110 L 81 110"></path>
<path className={props.className} stroke={props.stroke} fill="transparent"  d="M 113 186 L 113 186 L 107 188 L 100 191 L 91 196 L 87 199"></path>
</svg>

export default Sun
