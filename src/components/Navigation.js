import React from 'react'
import Link from './Link'
import simon from '../assets/simon.gif'

export default () =>
    <div className='nav'>
      <div className='flex items-center'>
        <Link className='nav-link' to='/work'>work</Link>
        <Link to='/'>
          <img 
            className='simon'
            src={simon} 
            alt='this is a cat'
          />
        </Link>
        <Link className='nav-link' to='/more'>more</Link>
      </div>
    </div>
