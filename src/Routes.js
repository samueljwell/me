import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from './views/Home'
import Work from './views/Work'
import Play from './views/Play'

export default () =>
  <Switch>
    <Route path='/' exact component={Home} />
    <Route path='/work' exact component={Work} />
    <Route path='/more' exact component={Play} />
    <Route component={Home} />
  </Switch>;

