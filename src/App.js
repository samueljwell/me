import React from 'react';
import Routes from './Routes'
import Navigation from './components/Navigation'
import './styles/App.css';

function App() {
  return (
    <div className="app">
      <div className="app-column">
        <Navigation />
        <Routes />
      </div>
    </div>
  );
}

export default App;
